#!/usr/bin/env node

var SENTINEL = '[this is the end]'

function writeout(text) {
  process.stdout.write(text+'\n')
}

function send_output(text) {
  process.stdout.write(text+'\n')
  process.stdout.write(SENTINEL+'\n')
}

send_output('node')

var vm = require('vm');

var codeblock = []

var ctx = vm.createContext();
process.stdin.resume();
process.stdin.setEncoding('utf8');

process.stdin.on('data', function(chunk) {
  chunk = chunk.replace(/(\n\r$)|(\r\n$)|(\n$)|(\r$)/, '')
  chunks = chunk.match(/[^\r\n]+/g);
  if (chunks[chunks.length-1] != SENTINEL) {
    codeblock.push(chunk);
  }
  else {
    for (var i = 0; i < (chunks.length - 1); i++)
    codeblock.push(chunks[i]);
    code = codeblock.join('\n')
    codeblock = []
    try {
      res = vm.runInContext(code,ctx)
      codeblock = []
      console.log(res)
    }
    catch (err) {
      console.log("Error:", err)
    }
    process.stdout.write(SENTINEL+'\n')
  }
});

