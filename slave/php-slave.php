<?php

$SENTINEL = "[this is the end]";

fwrite(STDOUT,"php ".phpversion()."\n");
fwrite(STDOUT,$SENTINEL."\n");
flush();

$codeblock = array();

while (true) {
  $line = trim(fgets(STDIN));
  if ($line != $SENTINEL) {
    array_push($codeblock,$line);
  }
  else {
    $code = implode("\n",$codeblock);
    $codeblock = array();
    $res = eval($code.";");
    if ( $res === false && ( $error = error_get_last() ) ) {
      echo "ERROR: ".$error['message']." on line ".$error['line'];
    echo "\n";
    }
    else {
    print_r($res);
    echo "\n";
    }
    fwrite(STDOUT,$SENTINEL."\n");
    flush();
  }

}

?>