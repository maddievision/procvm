#!/usr/bin/env ruby

# Derived from http://www.decalage.info/python/ruby_bridge
# Ruby doesn't really support context scopes for eval() but
# using code from http://blog.jayfields.com/2006/07/ruby-eval-with-binding.html
# we can get close by using instance variables of a class
# whose scope we can execute within.

SENTINEL = '[this is the end]'

def send_output(str)
  puts str
  puts SENTINEL
  STDOUT.flush
  STDERR.flush
end

class Test
  def theblock
    return Proc.new {}
  end
end

send_output('ruby '+RUBY_VERSION)

codeblock = []

proc = Test.new.theblock

while cmd = STDIN.gets
  cmd.chop!
  if cmd != SENTINEL
    codeblock << cmd
  else 
    code = codeblock.join("\n")
    codeblock = []
    begin
      p eval(code,proc.binding)
    rescue => msg
      puts "ERROR: "+msg.to_s
    end
    puts SENTINEL
    STDOUT.flush
    STDERR.flush
  end
end
