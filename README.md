This project is a Python project for hosting process-based VMs or interpreters.
It is largely based off code from http://www.decalage.info/python/ruby_bridge

The idea is the interpreter runs on a slave process which responds to usual 
REPL kind of input, and returns the output as well as a sentinel.

This is very much an experimental project and in its infancy.  I'm looking to add some
more interesting things such as jazz up the interprocess protocol a bit,
perhaps some interlangauge data transfer or function-level binding.

# node.js Example

    djbouche@tsumugi:~/code/testpipe > python2 -i procvm.py
    >>> node = ProcVM('slave/node-slave.js')
    >>> node
    <ProcVM: node>
    >>> node.eval("a = {hello: 'world'}")
    "{ hello: 'world' }"
    >>> node.eval("a.b = 2")
    '2'
    >>> node.eval("a")
    "{ hello: 'world', b: 2 }"
    >>> node.interactive()
    node>> c = [a, 3]
    [ { hello: 'world', b: 2 }, 3 ]
    node>> exit
    >>>

   
# Ruby Example

    djbouche@tsumugi:~/code/testpipe > python2 -i procvm.py
    >>> ruby = ProcVM('slave/ruby-slave.rb')
    >>> ruby
    <ProcVM: ruby 1.9.3>
    >>> ruby.interactive()
    ruby 1.9.3>> @a = 'some string'
    "some string"
    ruby 1.9.3>> @a.reverse
    "gnirts emos"
    ruby 1.9.3>> @x = [1,2,3,4].shuffle
    [3, 2, 4, 1]
    ruby 1.9.3>> exit
    >>>

# C# Example

    djbouche@tsumugi:~/code/testpipe > python2 -i procvm.py
    >>> cs = ProcVM('slave/cs-slave.exe')
    >>> cs
    <ProcVM: csharp>
    >>> cs.eval("int a = 4")
    ''
    >>> cs.eval("a + 4")
    '8'
    >>> cs.interactive()
    csharp>> Console.WriteLine("The number is {0}",a)
    The number is 4
    csharp>> exit
    >>>
    
