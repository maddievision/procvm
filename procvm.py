# And the python Proc VM host
# Derived from http://www.decalage.info/python/ruby_bridge
 
from subprocess import Popen, PIPE, STDOUT
 
SENTINEL = '[this is the end]'

class ProcVM(object):
    def __init__(self,*args):
        self.process = args
        self.active = False
        self.name = None
        self.slave = None
        self.boot()
    def boot(self):
        self.slave = Popen(self.process, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        self.name = self.get_output()
        self.active = True
    def kill(self):
        self.slave.kill
        self.active = False
    def send_input(self,msg):
        self.slave.stdin.write('%s\n%s\n' % (msg,SENTINEL))
    def get_output(self):
        result = []
        while True:
            if self.slave.poll() is not None:
                self.active = False
                return None
            # read one line, remove newline chars and trailing spaces:
            line = self.slave.stdout.readline().rstrip()
            #print 'line:', line
            if line == SENTINEL:
                break
            result.append(line)
        return '\n'.join(result)
    def eval(self,cmd):
        if not self.active: self.boot()
        self.send_input(cmd)
        return self.get_output()
    def interactive(self):
        if not self.active: self.boot()
        while True:
            # read user input, expression to be evaluated:
            line = raw_input("%s>> " % self.name)
            if line == 'exit': break
            print self.eval(line)
    def __repr__(self):
        return "<%s: %s>" % (self.__class__.__name__,self.name)
